import socket as ip

class ConnectionClosed(Exception):
	pass

class InPacket:
	def __init__(self, sock: ip.socket) -> None:
		size = 0
		position = 0
		while True:
			recv = sock.recv(1)
			if len(recv) == 0:
				raise ConnectionClosed()
			byte = recv[0]
			size |= (byte & 0x7F) << position
			if byte & 0x80 == 0:
				break
			position += 7

			if position >= 32:
				raise RuntimeError('Packet size is too big.')

		if size > 2048:
			raise RuntimeError(f'Packet size is too big ({size} bytes).')
		self.offset = 0
		self.data = sock.recv(size)
		self.id_ = self.read_varint()

	def read_varint(self) -> int:
		value = 0
		position = 0
		while True:
			byte = self.read_byte()
			value |= (byte & 0x7F) << position
			if byte & 0x80 == 0:
				break
			position += 7

			if position >= 32:
				raise RuntimeError('Variable-size int is too big.')
		return value

	def read_string(self) -> str:
		size = self.read_varint()
		return self.read_bytes(size).decode('utf-8')

	def read_bytes(self, count: int) -> bytes:
		start = self.offset
		self.seek(count)
		return self.data[start:self.offset]

	def read_byte(self) -> int:
		self.seek(1)
		return self.data[self.offset - 1]

	def seek(self, offset: int) -> None:
		# TODO check if enough size
		self.offset += offset

class OutPacket:
	def __init__(self, id_: int) -> None:
		self.data = bytearray()
		self.id_ = id_
		self.write_varint(id_)

	def send(self, sock: ip.socket) -> None:
		all_data = bytearray()
		size = len(self.data)
		while True:
			if size & ~0x7F == 0:
				all_data.append(size)
				break
			all_data.append(size)
			if size < 0:
				size += 0x100000000
			size >>= 7

		all_data.extend(self.data)
		sock.send(all_data)

	def write_varint(self, value: int) -> None:
		while True:
			if value & ~0x7F == 0:
				self.write_byte(value)
				break
			self.write_byte((value & 0x7F) | 0x80)
			if value < 0:
				value += 0x100000000
			value >>= 7

	def write_string(self, value: str) -> None:
		self.write_varint(len(value))
		self.data.extend(value.encode('utf-8'))

	def write_bytes(self, data: bytes) -> None:
		self.data.extend(data)

	def write_byte(self, val: int) -> None:
		self.data.append(val)
