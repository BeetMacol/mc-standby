from pathlib import Path
from .server_wrapper import ServerWrapper
from .cli import interactive_config

def main():
	config, start_now = interactive_config()
	wrapper = ServerWrapper(Path(), config)
	if start_now:
		wrapper.start_server()
	while wrapper.run():
		continue

if __name__ == '__main__':
	main()
