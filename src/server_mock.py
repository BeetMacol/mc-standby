from uuid import UUID
from dataclasses import dataclass
import socket as ip
from .mc_protocol import InPacket, OutPacket, ConnectionClosed

STATUS_RESPONSE = '''{
	"version": {
		"name": "standby",
		"protocol": $protocol_ver$
	},
	"players": {
		"max": 1,
		"online": 0
	},
	"description": $motd$,
	"enforcesSecureChat": false,
	"previewsChat": false
}'''

@dataclass
class ServerMockConfig:
	endpoint: tuple[str, int] = ('0.0.0.0', 25565)
	whitelist: set[UUID] | None = None
	motd_json: str = '{"text": "The server is currently in stand-by mode."}'
	msg_ignore_json: str = '{"text": "You are not whitelisted."}'
	msg_accept_json: str = '{"text": "Starting server... Come back in a few minutes."}'

class ServerMock:
	def __init__(self, config: ServerMockConfig) -> None:
		self.config = config

	def handshake(self, client: ip.socket) -> tuple[bool, int]:
		# Handshake
		packet = InPacket(client)
		if packet.id_ != 0:
			raise RuntimeError('Packet expected to be a Handshake (0:0x00)')
		protocol_ver = packet.read_varint()
		packet.read_string()
		packet.seek(2)
		next_state = packet.read_varint()
		return next_state == 2, protocol_ver

	def status(self, client: ip.socket, protocol_ver: int) -> None:
		# Status Request
		packet = InPacket(client)
		if packet.id_ != 0:
			raise RuntimeError('Packet expected to be a Status Request (1:0x00)')

		# Status Response
		packet = OutPacket(0)
		packet.write_string(STATUS_RESPONSE
			.replace('$protocol_ver$', str(protocol_ver), 1)
			.replace('$motd$', self.config.motd_json, 1))
		packet.send(client)

		# Ping Request
		packet = InPacket(client)
		if packet.id_ != 1:
			return
		payload = packet.read_bytes(8)

		# Pong Response
		packet = OutPacket(1)
		packet.write_bytes(payload)
		packet.send(client)

	def login(self, client: ip.socket) -> bool:
		# Login Start
		packet = InPacket(client)
		if packet.id_ != 0:
			raise RuntimeError('Packet expected to be a Login Start (2:0x00)')
		packet.read_string()
		uuid = UUID(bytes=packet.read_bytes(16))
		if self.config.whitelist is not None and uuid not in self.config.whitelist:
			# Disconnect
			packet = OutPacket(0)
			packet.write_string(self.config.msg_ignore_json)
			packet.send(client)
			return False

		# Disconnect
		packet = OutPacket(0)
		packet.write_string(self.config.msg_accept_json)
		packet.send(client)
		return True

	def close_client(self, client: ip.socket) -> None:
		try:
			# skip client fin to avoid TIME_WAIT
			while len(client.recv(1024)) > 0:
				pass
		except:
			pass
		client.close()

	def run(self) -> bool:
		sock = ip.socket(ip.AF_INET, ip.SOCK_STREAM, 0)
		sock.setsockopt(ip.SOL_SOCKET, ip.SO_REUSEPORT, True)
		sock.bind(self.config.endpoint)
		sock.listen()
		while True:
			client: ip.socket | None = None
			try:
				client, endpoint = sock.accept()
				print(f'Accepted new connection from "{endpoint[0]}:{endpoint[1]}".')
				next_login, protocol_ver = self.handshake(client)
				if not next_login:
					self.status(client, protocol_ver)
				else:
					if self.login(client):
						self.close_client(client)
						sock.close()
						return True
				self.close_client(client)
			except RuntimeError as error:
				print(error)
				if client:
					self.close_client(client)
			except ConnectionClosed:
				continue
			except BrokenPipeError:
				continue
			except KeyboardInterrupt:
				if client:
					self.close_client(client)
				sock.close()
				return False
