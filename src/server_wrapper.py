from time import sleep
from dataclasses import dataclass
from pathlib import Path
from .server_mock import ServerMock, ServerMockConfig
from .server_checker import ServerChecker, ServerCheckerConfig, ServerPinger
from .console import create_console

@dataclass
class ServerWrapperConfig:
	mock_config: ServerMockConfig
	checker_config: ServerCheckerConfig
	wrapper_stop_server: bool = True
	# TODO server_stop_wrapper (for ConnectionClosed)
	check_countdown: int = 120
	start_cmd: str = './run.sh'
	stop_cmd: str = 'stop'
	stop_delay: int = 30

class ServerWrapper:
	def __init__(self, directory: Path, config: ServerWrapperConfig) -> None:
		if not directory.exists() or not directory.is_dir():
			raise RuntimeError(f'Path "{directory}" must be a directory.')
		self.directory = directory
		self.config = config
		self.mock = ServerMock(self.config.mock_config)
		self.checker = self.config.checker_config.create_checker()
		self.console = create_console(directory)
		self._started = False

	def start_server(self) -> None:
		print('Starting server.')
		self.console.cmd(self.config.start_cmd)
		self._started = True

	def stop_server(self) -> None:
		print('Stopping server.')
		self.console.cmd(self.config.stop_cmd)
		self._started = False

	# returns whether to continue program
	def run(self) -> bool:
		if not self._started:
			print('Starting server mock.')
			if not self.mock.run():
				return False
			self.start_server()
		countdown = self.config.check_countdown + 10
		while countdown > 10:
			try:
				sleep(countdown / 2)
			except KeyboardInterrupt:
				if self.config.wrapper_stop_server:
					self.stop_server()
				return False
			if self.checker.has_players():
				if countdown != self.config.check_countdown + 10:
					print('Server is no longer empty.')
					countdown = self.config.check_countdown + 10
				continue
			countdown /= 2
			if countdown > 10:
				print(f'Server is empty. Stopping in {countdown - 10} seconds.')
		self.stop_server()
		sleep(self.config.stop_delay)
		return True
