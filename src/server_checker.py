from abc import ABC, abstractmethod
from dataclasses import dataclass
import socket as ip
import re
from .mc_protocol import InPacket, OutPacket, ConnectionClosed

class ServerChecker(ABC):
	@abstractmethod
	def has_players(self) -> bool: ...

class ServerCheckerConfig(ABC):
	@abstractmethod
	def create_checker(self) -> ServerChecker: ...

@dataclass
class ServerPingerConfig(ServerCheckerConfig):
	endpoint: tuple[str, int] = ('localhost', 25565)

	def create_checker(self) -> ServerChecker:
		return ServerPinger(self)

class ServerPinger(ServerChecker):
	def __init__(self, config: ServerPingerConfig) -> None:
		self.config = config

	def has_players(self) -> bool:
		sock = ip.socket(ip.AF_INET, ip.SOCK_STREAM, 0)
		sock.connect(self.config.endpoint)

		# Handshake
		packet = OutPacket(0)
		packet.write_varint(-1)
		packet.write_string(self.config.endpoint[0])
		packet.write_bytes(self.config.endpoint[1].to_bytes(2))
		packet.write_varint(1)
		packet.send(sock)

		# Status Request
		packet = OutPacket(0)
		packet.send(sock)

		# Status Response
		packet = InPacket(sock)
		if packet.id_ != 0:
			raise RuntimeError('Packet expected to be a Status Response (0:0x00)')
		json = packet.read_string()

		sock.close()
		match = re.search(r'"online": ?([0-9]+)', json)
		if not match:
			raise RuntimeError('Bad server response format (could not find online count).')
		return int(match.group(1)) > 0
