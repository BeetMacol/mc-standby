from typing import Callable
from .server_mock import ServerMockConfig
from .server_checker import ServerCheckerConfig, ServerPingerConfig
from .server_wrapper import ServerWrapperConfig

def interactive_ask(question: str, default: str, return_default: bool = True) -> str:
	response = input(f'{question} [{default}]: ').strip()
	if not response and return_default:
		response = default
	return response

def interactive_bool(question: str, default: bool) -> bool:
	response = interactive_ask(question, 'yes' if default else 'no', False)
	if not response:
		return default
	return response.lower() == 'yes'

def interactive_config(
	defaults: ServerWrapperConfig = ServerWrapperConfig(ServerMockConfig(), ServerPingerConfig())
) -> tuple[ServerWrapperConfig, bool]:
	config = ServerWrapperConfig(ServerMockConfig(), ServerPingerConfig())

	config.mock_config.endpoint = (
		interactive_ask(
			'What IP address would you like to use for the server mock?',
			defaults.mock_config.endpoint[0]
		),
		int(interactive_ask(
			'What TCP port would you like to use for the server mock?',
			str(defaults.mock_config.endpoint[1])
		))
	)
	# TODO whitelist?
	config.mock_config.motd_json = interactive_ask(
		'What message to display on the server mock\'s MOTD?',
		defaults.mock_config.motd_json
	)
	if config.mock_config.whitelist:
		config.mock_config.msg_ignore_json = interactive_ask(
			'What message to show to non-whitelisted players?',
			defaults.mock_config.msg_ignore_json
		)
	config.mock_config.msg_accept_json = interactive_ask(
		'What message to show to the player after initiating the server?',
		defaults.mock_config.msg_accept_json
	)

	available_checkers: list[tuple[str, ServerCheckerConfig]] = [
		('server_pinger', ServerPingerConfig())
	]
	print('Available checkers:')
	default_checker: tuple[int, ServerCheckerConfig] \
		= (0, next(iter(available_checkers))[1])
	for idx, checker in enumerate(available_checkers):
		print(f'{idx}. {checker[0]}')
		if isinstance(checker[1], type(defaults.checker_config)):
			default_checker = (idx, defaults.checker_config)
	checker_idx = interactive_ask(
		'What checker would you like to use?',
		str(default_checker[0]), True
	)
	if checker_idx:
		config.checker_config = available_checkers[int(checker_idx)][1]
		default_checker = (int(checker_idx), config.checker_config)
	else:
		config.checker_config = default_checker[1]
	checker_name = available_checkers[default_checker[0]][0]
	if checker_name == 'server_pinger':
		config.checker_config = ServerPingerConfig()
		if interactive_bool(
			'Reuse mock\'s endpoint (address and port) for server pinger?', True
		):
			config.checker_config.endpoint = config.mock_config.endpoint
		else:
			assert(isinstance(default_checker[1], ServerPingerConfig))
			config.checker_config.endpoint = (
				interactive_ask(
					'What IP address to ping for player list?',
					default_checker[1].endpoint[0]
				),
				int(interactive_ask(
					'What IP address to ping for player list?',
					str(default_checker[1].endpoint[1])
				))
			)
	else:
		print(
			'This checker is unsupported by the interactive config. '
			'Default or previous values will be used.'
		)

	config.wrapper_stop_server = interactive_bool(
		'Should the server stop with the wrapper?', defaults.wrapper_stop_server
	)
	config.check_countdown = int(interactive_ask(
		'About how many seconds to wait before stopping the empty server?',
		str(defaults.check_countdown)
	))
	config.start_cmd = interactive_ask(
		'What command to use for server startup?', defaults.start_cmd
	)
	config.stop_cmd = interactive_ask(
		'What command to use for server stopping?', defaults.stop_cmd
	)
	config.stop_delay = int(interactive_ask(
		'How much time to wait after server stop (to make sure socket is closed)?',
		str(defaults.stop_delay)
	))

	start_now = interactive_bool('Start server now?', False)
	return config, start_now
