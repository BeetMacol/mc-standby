from pathlib import Path
from .console import ServerConsole
from .tmux import TmuxServerConsole

def create_console(directory: Path) -> ServerConsole:
	return TmuxServerConsole(directory)
