from abc import ABC, abstractmethod

class ServerConsole(ABC):
	@abstractmethod
	def cmd(self, command: str) -> None: ...
