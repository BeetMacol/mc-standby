from pathlib import Path
import libtmux as tmux
from .console import ServerConsole

class TmuxServerConsole(ServerConsole):
	def __init__(self, directory: Path, session_name: str | None = None) -> None:
		self.server = tmux.Server()
		start_dir: str | None = str(directory) if directory else None
		self.session = self.server.new_session(session_name, start_directory=start_dir, window_name='mc-server')
		self.pane = self.session.windows[0].panes[0]

	def cmd(self, command: str) -> None:
		self.pane.send_keys(command)
