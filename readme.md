# Minecraft "Standby" Server Wrapper

This is a simple wrapper script for Minecraft servers that starts and stops the server automatically depending on player-based demand. All Minecraft versions since 1.7 are supported (though not all were tested).

The script introduces a *standby* mode, during which the server is turned off and instead of itself, a server list mock is running to allow for interaction with players.  Whenever a player attempts to joine the server while it's in the *standby* mode, the script initiates the actual server. Similarly, whenever the server is empty for a configured amount of time, the server is stopped and replaced with the server list mock indicating the *standby* mode.

Administrators of small servers may find this useful as it prevents wasting system resources and energy when the server is empty. Simply run the program in the Minecraft server directory and follow the interactive instructions.

The program is written in the Python language and can also be used as a library.

## TODO before first release

- configuration locaing
- interactive configuration
  - ~~the interactive questions~~
  - optionally saving the configuration
  - forcing interactive mode even when config exists (-i)
- option to simply create the default config file (-c)
- make tmux optional
- some tmux alternative(s)
  - something that also works on Windows
  - simply running as a subprocess?
- minecraft whitelist parsing
- option to include whitelist for stopping

## Licensing

This project is copyrighted as: **Copyright (c) 2023 BeetMacol**.

The software is licensed under **The BSD 3-Clause "New" or "Revised" License** – an [OSI-approved](https://opensource.org/license/bsd-3-clause/) **open-source** license. Full text of the license is available in the [license](license) file.

## Contributing

Code contributions are welcome. Any code added to this repository will be available under the 3-Clause BSD license (mentioned above).

If you find any bugs, have a feature request or would like to ask a question, feel free to [create an issue](https://gitlab.com/BeetMacol/mc-standby/-/issues). It might also be useful to search [issues already added by other users](https://gitlab.com/BeetMacol/mc-standby/-/issues).

## Installation

### Dependencies

Before installation, you will need a [Python interpreter](https://www.python.org/downloads/) for at least version 3.9. For the installation methods listed below, [PIP](https://pypi.org/project/pip/) will also be needed (it is usually bundled with the Python installation).

It is also recommended to install [tmux](https://tmux.github.io/) for simpler terminal management. If you have tmux, you must also install a related Python package ([libtmux](https://libtmux.git-pull.com/)) to use it with this program:
```
python3 -m pip install --user libtmux
```

### Full release (from PyPi)

To install the latest release of the program, simply run:
```
python3 -m pip install --user mc-standby
```

To install another version, append `==<version>` to the above command without any spaces.

### Latest changes (from Git)

To install a version that isn't a full release (for example, including **changes that will be added to a future release**) you will need to download the source code from the repository.

First, obtain the source code for the desired version. If you need the latest version, either download the source code available under the `Code` button in [the repository](https://gitlab.com/BeetMacol/mc-standby) or run:
```
git clone https://gitlab.com/BeetMacol/mc-standby.git
```
If you need another version, you may need to use git to navigate to an older commit.

Second, change to the downloaded directory (`cd mc-standby`) and run:
```
python3 -m pip install --user .
```
